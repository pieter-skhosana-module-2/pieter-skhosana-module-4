import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Login",
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          //input fields for username and password
          children: <Widget>[
            const Padding(
              padding: EdgeInsets.all(15),
              child: TextField(
                decoration: InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: "Username",
                  icon: Icon(
                    Icons.perm_identity,
                    color: Colors.deepPurpleAccent,
                  ),
                ),
              ),
            ),
            const Padding(
              padding: EdgeInsets.all(15),
              child: TextField(
                obscureText: true,
                decoration: InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: "Password",
                  icon: Icon(
                    Icons.key,
                    color: Colors.deepPurpleAccent,
                  ),
                ),
              ),
            ),
            const Padding(
              padding: EdgeInsets.all(0),
              child: Checkbox(
                value: false,
                onChanged: (null),
              ),
            ),
            const Text("Remember Me",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                )),
            //Login button
            const SizedBox(height: 50),
            ElevatedButton(
              onPressed: () => {},
              child: const Text(
                "Go",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              style: ElevatedButton.styleFrom(
                fixedSize: const Size(300, 30),
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                ),
              ),
            ),
            //register button
            const SizedBox(height: 50),
            ElevatedButton(
              onPressed: () => {},
              child: const Text(
                "Register",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              style: ElevatedButton.styleFrom(
                fixedSize: const Size(300, 30),
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.settings),
        onPressed: () {},
      ),
    );
  }
}

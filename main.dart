import 'package:flutter/material.dart';
import 'splashscreen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      //remove the debug banner
      debugShowCheckedModeBanner: false,
      //Display the spalsh screen before the home page
      home: const SplashScreen(),
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.blueGrey)
            .copyWith(secondary: Colors.deepPurpleAccent),
        textTheme: const TextTheme(
            bodyText2: TextStyle(color: Color.fromARGB(255, 48, 24, 134))),
      ),
    );
  }
}
